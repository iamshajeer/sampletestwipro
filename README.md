# Android Sample Assignment

This POC application loads data from REST API and displays photos with headings and descriptions in a list. application also supports swipe refresh, multiple orientation... also application uses the below mentioned libraries. 


#### Version 1.0.1 (10/10/2018)



## Please find the file named Android Proficiency Exercise.pdf which contains the details about the application.

## Architectural pattern used
This application uses MVP pattern. MVP helps to separate and define a clear single role for each component. also makes testable.


## Important Dependencies Used

- Dagger, Dagger aims to address many of the development and performance issues that have plagued reflection-based solutions.
	- ```NetworkModule``` and ```ApplicationModule``` are the two modules created in this application. ```NetworkModule``` provides Network connection instance to make API calls, also ```ApplicationModule``` helps to inject ```Application Context``` wherever required
- Butterknife, Butterknife is a light weight library to inject views into Android components. It uses annotation processing.
	- Butterknife used for UI component injection. all the UI components are injected to application using butterknife
- Retrofit, Retrofit is a network library used in this application.
- Picasso, Picasso is used for loading images.
- Gson, the JSON deserializer we will use to transform the HTTP response into java models.

## Testing Framework Used

- [Mockito](https://site.mockito.org/) is used as testing framework in this application. mockito helps to mock classes.
