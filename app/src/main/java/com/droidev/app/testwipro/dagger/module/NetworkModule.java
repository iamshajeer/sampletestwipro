package com.droidev.app.testwipro.dagger.module;

import android.annotation.SuppressLint;

import com.droidev.app.testwipro.BuildConfig;
import com.droidev.app.testwipro.dagger.TestAppScope;
import com.droidev.app.testwipro.remote.ApiInterface;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@TestAppScope
public class NetworkModule {

    private static final long READ_TIMEOUT = 30;
    private static final long CONNECTION_TIMEOUT = 30;

    @Provides
    @TestAppScope
    public ApiInterface getApiInterface(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiInterface.class);
    }

    @Provides
    @TestAppScope
    OkHttpClient getOkHttpClient(HttpLoggingInterceptor interceptor, X509TrustManager trustAllCert) {
        OkHttpClient okHttpClient = null;
        try {
            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder().readTimeout
                    (READ_TIMEOUT, TimeUnit.SECONDS).connectTimeout
                    (CONNECTION_TIMEOUT, TimeUnit.SECONDS);

            okHttpClientBuilder.sslSocketFactory((SSLSocketFactory) SSLSocketFactory.getDefault(), trustAllCert);
            okHttpClientBuilder.addInterceptor(interceptor);
            okHttpClient = okHttpClientBuilder.build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return okHttpClient;
    }

    @Provides
    @TestAppScope
    X509TrustManager getAllTrust() {
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @SuppressLint("TrustAllX509TrustManager")
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                    }

                    @SuppressLint("TrustAllX509TrustManager")
                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };
        return (X509TrustManager) trustAllCerts[0];
    }

    @Provides
    @TestAppScope
    HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level loggingLevel = (BuildConfig.DEBUG)
                ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;
        interceptor.setLevel(loggingLevel);
        return interceptor;
    }
}
