package com.droidev.app.testwipro.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * This util class handles all the toast message related operations
 */
public class ToastUtil {

    /**
     * Shows toast message
     *
     * @param context Caller context
     * @param message message to be toasted
     */
    public static void showToast(Context context, String message) {

        if (!CommonUtils.isEmpty(message)) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}
