package com.droidev.app.testwipro.presenter;

import android.content.Context;

import com.droidev.app.testwipro.base.TestApplication;
import com.droidev.app.testwipro.contract.MainFragmentContract;
import com.droidev.app.testwipro.remote.NetworkAdapter;
import com.droidev.app.testwipro.remote.ResponseCallback;
import com.droidev.app.testwipro.remote.response.CountryResponseWrapper;
import com.droidev.app.testwipro.remote.response.ResponseError;
import com.droidev.app.testwipro.utils.NetworkCheckUtil;
import com.droidev.app.testwipro.view.MainView;

import javax.inject.Inject;

public class MainFragmentPresenter implements MainFragmentContract {
    private MainView mView;
    @Inject
    Context mContext;
    private CountryResponseWrapper mCountryResponse;

    public MainFragmentPresenter(MainView mainView) {
        mView = mainView;
        TestApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void getCountryDetails(boolean shouldShowProgress) {

        if (mCountryResponse == null) {
            if (NetworkCheckUtil.isNetworkAvailable(mContext)) {
                if (shouldShowProgress) {
                    mView.showProgress();
                }
                NetworkAdapter.get().getTestData(new ResponseCallback<CountryResponseWrapper>() {
                    @Override
                    public void onSuccess(CountryResponseWrapper countryResponseWrapper) {
                        if (shouldShowProgress) {
                            mView.hideProgress();
                        }
                        mCountryResponse = countryResponseWrapper;
                        mView.populateCountryDetails(countryResponseWrapper);
                    }

                    @Override
                    public void onFailure(ResponseError responseError) {
                        if (shouldShowProgress) {
                            mView.hideProgress();
                        }
                        mView.handleError(responseError);
                    }
                });
            } else {
                mView.showNoNetworkAlert();
            }
        } else {
            mView.populateCountryDetails(mCountryResponse);
        }
    }
}
