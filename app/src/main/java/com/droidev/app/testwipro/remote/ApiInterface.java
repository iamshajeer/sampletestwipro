package com.droidev.app.testwipro.remote;

import com.droidev.app.testwipro.remote.response.CountryResponseWrapper;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * This interface defines all the network endpoints
 */
public interface ApiInterface {

    @GET("/s/2iodh4vg0eortkl/facts.json")
    Call<CountryResponseWrapper> getCountryDetails();

}
