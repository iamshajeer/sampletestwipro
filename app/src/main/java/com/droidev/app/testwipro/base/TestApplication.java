package com.droidev.app.testwipro.base;

import android.app.Application;

import com.droidev.app.testwipro.dagger.DaggerTestAppComponent;
import com.droidev.app.testwipro.dagger.TestAppComponent;

public class TestApplication extends Application {

    private static TestApplication mInstance;
    private TestAppComponent mAppComponent;

    public TestApplication() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerTestAppComponent
                .builder()
                .application(this)
                .build();
    }

    public static TestApplication getInstance() {
        return mInstance;
    }

    public TestAppComponent getAppComponent() {
        return mAppComponent;
    }
}
