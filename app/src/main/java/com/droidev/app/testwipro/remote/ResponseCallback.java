package com.droidev.app.testwipro.remote;

import com.droidev.app.testwipro.remote.response.ResponseError;

public interface ResponseCallback<T> {

    void onSuccess(T t);

    void onFailure(ResponseError responseError);

}
