package com.droidev.app.testwipro.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;

/**
 * Check link <a href="https://gist.github.com/rajohns08/e241e087d9a780e82eb6">Check link for details</a>
 */
public class ProgressUtil {

    private static ProgressDialog sProgressDialog;

    public static void show(Context context, int messageResourceId) {
        if (sProgressDialog != null) {
            sProgressDialog.dismiss();
        }
        int style;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            style = android.R.style.Theme_Material_Light_Dialog;
        } else {
            style = ProgressDialog.THEME_HOLO_LIGHT;
        }
        sProgressDialog = new ProgressDialog(context, style);
        sProgressDialog.setMessage(context.getResources().getString(messageResourceId));
        sProgressDialog.setCancelable(false);
        sProgressDialog.show();
    }

    public static void dismiss() {
        if (sProgressDialog != null && sProgressDialog.isShowing()) {
            sProgressDialog.dismiss();
            sProgressDialog = null;
        }
    }
}