package com.droidev.app.testwipro.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkCheckUtil {

    /**
     * Checks if device is connected to network or not
     *
     * @param context caller context
     * @return true if network connected or false
     */
    public static Boolean isNetworkAvailable(Context context) {
        Boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();
            if (networkInfo != null) {
                for (NetworkInfo aNetworkInfo : networkInfo) {
                    if (aNetworkInfo.isConnected()) {
                        isConnected = true;
                        break;
                    }
                }
            }
        }
        return isConnected;
    }

}
