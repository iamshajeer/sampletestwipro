package com.droidev.app.testwipro.view;

import com.droidev.app.testwipro.base.BaseView;
import com.droidev.app.testwipro.remote.response.CountryResponseWrapper;

public interface MainView extends BaseView {

    void populateCountryDetails(CountryResponseWrapper countryResponseWrapper);
}
