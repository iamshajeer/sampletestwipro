package com.droidev.app.testwipro.ui;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droidev.app.testwipro.R;
import com.droidev.app.testwipro.contract.MainFragmentContract;
import com.droidev.app.testwipro.presenter.MainFragmentPresenter;
import com.droidev.app.testwipro.remote.response.CountryResponseWrapper;
import com.droidev.app.testwipro.remote.response.ResponseError;
import com.droidev.app.testwipro.ui.adapter.CountryListAdapter;
import com.droidev.app.testwipro.ui.model.ListItemCountry;
import com.droidev.app.testwipro.utils.CommonUtils;
import com.droidev.app.testwipro.utils.ProgressUtil;
import com.droidev.app.testwipro.view.MainView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends ToolbarFragment implements MainView {

    public static final String TAG = MainFragment.class.getSimpleName();
    private MainFragmentContract mContract;
    private CountryListAdapter mAdapter;

    @BindView(R.id.contact_list)
    RecyclerView mContactList;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.txt_empty_view)
    TextView mEmptyView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mContract = new MainFragmentPresenter(this);
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initToolbar(view);
        initSwipeControl();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListProperties();
        mContract.getCountryDetails(true);
    }

    private void setListProperties() {
        mAdapter = new CountryListAdapter(null);
        mContactList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContactList.setHasFixedSize(true);
        mContactList.setAdapter(mAdapter);
    }

    @Override
    public void populateCountryDetails(CountryResponseWrapper responseWrapper) {
        showCountryDetails(responseWrapper);
    }

    private void showCountryDetails(CountryResponseWrapper responseWrapper) {
        if (responseWrapper != null) {
            if (!responseWrapper.getCountryResponses().isEmpty()) {
                mContactList.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
                mAdapter.setData(ListItemCountry.getItemListFrom(responseWrapper.getCountryResponses()));
                mAdapter.notifyDataSetChanged();
            } else {
                showEmptyView();
            }
            setTitle(responseWrapper);
        } else {
            showEmptyView();
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void showEmptyView() {
        mContactList.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
    }

    private void setTitle(CountryResponseWrapper responseWrapper) {
        if (responseWrapper != null) {
            String title = responseWrapper.getTitle();
            if (!CommonUtils.isEmpty(title) && getView() != null) {
                setToolbarTitle(title);
            }
        }
    }

    private void initSwipeControl() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mSwipeRefreshLayout.setRefreshing(true);
            mContract.getCountryDetails(false);
        });
    }

    @Override
    public void showProgress() {
        ProgressUtil.show(getActivity(), R.string.loading);
    }

    @Override
    public void hideProgress() {
        ProgressUtil.dismiss();
    }

    @Override
    public void handleError(ResponseError responseError) {
        disableSwipeRefresh();
        showResponseErrorDialog(responseError);
    }

    @Override
    public void showNoNetworkAlert() {
        showEmptyView();
        disableSwipeRefresh();
        showNoNetworkDialog();
    }

    private void disableSwipeRefresh() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}
