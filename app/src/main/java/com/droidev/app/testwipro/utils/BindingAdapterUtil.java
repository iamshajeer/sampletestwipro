package com.droidev.app.testwipro.utils;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * All the data binding related operations written here
 */
public class BindingAdapterUtil {

    /**
     * Binding adapter used to show images in imageView using Picasso library
     *
     * @param imageView imageView to be bounded
     * @param url       image url
     */
    @BindingAdapter({"bind:src"})
    public static void setImageViewResource(ImageView imageView, String url) {
        Picasso.get()
                .load(url)
                .into(imageView);
    }

    /**
     * setting text in a text view with the contents trimmed
     *
     * @param textView text view to be used
     * @param text     text to set
     */
    @BindingAdapter({"bind:text"})
    public static void setText(TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text.trim());
        } else {
            textView.setText("");
        }
    }
}
