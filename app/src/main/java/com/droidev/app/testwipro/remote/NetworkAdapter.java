package com.droidev.app.testwipro.remote;

import com.droidev.app.testwipro.base.TestApplication;
import com.droidev.app.testwipro.remote.response.CountryResponseWrapper;

import javax.inject.Inject;

import retrofit2.Call;

public class NetworkAdapter {

    @Inject
    ApiInterface mApiProvider;

    private static NetworkAdapter sNetworkAdapter;

    private NetworkAdapter() {
        TestApplication.getInstance().getAppComponent().inject(this);
    }

    public static NetworkAdapter get() {
        if (sNetworkAdapter == null) {
            sNetworkAdapter = new NetworkAdapter();
        }
        return sNetworkAdapter;
    }

    public void getTestData(ResponseCallback<CountryResponseWrapper> responseCallback) {
        Call<CountryResponseWrapper> call = mApiProvider.getCountryDetails();
        call.enqueue(new RestCallback<>(responseCallback));
    }
}
