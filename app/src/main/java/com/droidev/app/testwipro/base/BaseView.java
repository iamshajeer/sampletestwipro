package com.droidev.app.testwipro.base;

import com.droidev.app.testwipro.remote.response.ResponseError;

public interface BaseView {

    void showProgress();

    void hideProgress();

    void handleError(ResponseError responseError);

    void showNoNetworkAlert();
}
