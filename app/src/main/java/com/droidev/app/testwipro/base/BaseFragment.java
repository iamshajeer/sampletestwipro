package com.droidev.app.testwipro.base;

import android.support.v4.app.Fragment;

import com.droidev.app.testwipro.R;
import com.droidev.app.testwipro.remote.response.ResponseError;
import com.droidev.app.testwipro.utils.ToastUtil;

/**
 * BaseFragment, All the fragment must extend from this
 */
public class BaseFragment extends Fragment {

    protected void showResponseErrorDialog(ResponseError responseError) {
        ToastUtil.showToast(getActivity(), responseError.getError());
    }

    protected void showNoNetworkDialog() {
        ToastUtil.showToast(getActivity(), getString(R.string.no_network));
    }
}
