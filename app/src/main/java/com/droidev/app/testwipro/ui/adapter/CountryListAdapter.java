package com.droidev.app.testwipro.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.droidev.app.testwipro.databinding.ListItemCountryBinding;
import com.droidev.app.testwipro.remote.response.CountryResponse;
import com.droidev.app.testwipro.ui.model.ListItem;
import com.droidev.app.testwipro.ui.model.ListItemCountry;

import java.util.List;


public class CountryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListItem> mListItems;

    public CountryListAdapter(List<ListItem> listItems) {
        mListItems = listItems;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case ListItem.LIST_ITEM_CONTACT:
                LayoutInflater layoutInflater =
                        LayoutInflater.from(viewGroup.getContext());
                ListItemCountryBinding itemContactBinding = ListItemCountryBinding.inflate(layoutInflater, viewGroup, false);
                return new ContactHolder(itemContactBinding);
            default:
                throw new RuntimeException("Unsupported View Type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ListItem listItem = mListItems.get(position);
        switch (listItem.getType()) {
            case ListItem.LIST_ITEM_CONTACT:
                ContactHolder contactHolder = (ContactHolder) viewHolder;
                contactHolder.bind(((ListItemCountry) listItem).getCountry());
                break;
            default:
                //nothing to do
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mListItems == null ? 0 : mListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mListItems == null ? 0 : mListItems.get(position).getType();
    }

    public void setData(List<ListItem> data) {
        mListItems = data;
    }

    public class ContactHolder extends RecyclerView.ViewHolder {
        private ListItemCountryBinding mBinding;

        ContactHolder(ListItemCountryBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(CountryResponse item) {
            mBinding.setCountry(item);
        }
    }
}
