package com.droidev.app.testwipro.remote.response;

import android.databinding.BaseObservable;

import com.google.gson.annotations.SerializedName;

public class CountryResponse extends BaseObservable {

    @SerializedName("description")
    private String description;
    @SerializedName("title")
    private String title;
    @SerializedName("imageHref")
    private String thumbnail;

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
