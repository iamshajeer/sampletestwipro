package com.droidev.app.testwipro.contract;

public interface MainFragmentContract extends BaseContract {

    void getCountryDetails(boolean shouldShowProgress);

}
