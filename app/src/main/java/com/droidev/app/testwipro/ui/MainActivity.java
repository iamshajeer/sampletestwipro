package com.droidev.app.testwipro.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.droidev.app.testwipro.R;
import com.droidev.app.testwipro.base.BaseActivity;

/**
 * MainActivity, launcher activity, this activity is responsible for all the operations in launcher screen
 */
public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadHomeFragment();
    }

    private void loadHomeFragment() {
        MainFragment mainFragment;
        FragmentManager fm = getSupportFragmentManager();
        mainFragment = (MainFragment) fm.findFragmentByTag(MainFragment.TAG);

        // create the fragment and data the first time
        if (mainFragment == null) {
            mainFragment = MainFragment.newInstance();
            addFragment(R.id.fragment_container,
                    mainFragment,
                    MainFragment.TAG);
        }
    }
}
