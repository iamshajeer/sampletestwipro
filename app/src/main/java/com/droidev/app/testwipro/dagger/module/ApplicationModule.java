package com.droidev.app.testwipro.dagger.module;

import android.app.Application;
import android.content.Context;

import com.droidev.app.testwipro.dagger.TestAppScope;

import dagger.Module;
import dagger.Provides;

@Module
@TestAppScope
public class ApplicationModule {

    @Provides
    Context provideContext(Application application) {
        return application;
    }
}
