package com.droidev.app.testwipro.remote.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountryResponseWrapper {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("rows")
    private List<CountryResponse> mCountryResponses;

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setCountryResponses(List<CountryResponse> countryResponses) {
        mCountryResponses = countryResponses;
    }

    public String getTitle() {
        return mTitle;
    }

    public List<CountryResponse> getCountryResponses() {
        return mCountryResponses;
    }
}
