package com.droidev.app.testwipro.ui;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.droidev.app.testwipro.R;
import com.droidev.app.testwipro.base.BaseFragment;
import com.droidev.app.testwipro.utils.CommonUtils;

/**
 * ToolbarFragment handles all the fragment related operations
 */
public class ToolbarFragment extends BaseFragment {

    protected void initToolbar(View view) {
        if (getActivity() != null) {
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        }
    }

    protected void setToolbarTitle(String title) {
        if (!CommonUtils.isEmpty(title) && getActivity() != null && ((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }
    }
}
