package com.droidev.app.testwipro.utils;

import android.text.TextUtils;

public class CommonUtils {

    /**
     * Checks if the provided string is empty or not
     *
     * @param text text to be checked
     * @return true if string is empty or false
     */
    public static boolean isEmpty(String text) {
        return TextUtils.isEmpty(text);
    }
}
