package com.droidev.app.testwipro.ui.model;

import com.droidev.app.testwipro.remote.response.CountryResponse;
import com.droidev.app.testwipro.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class ListItemCountry implements ListItem {

    private CountryResponse mContact;

    public CountryResponse getCountry() {
        return mContact;
    }

    public void setContact(CountryResponse contact) {
        mContact = contact;
    }

    public static ArrayList<ListItem> getItemListFrom(List<CountryResponse> contactList) {
        ArrayList<ListItem> listItemContacts = new ArrayList<>(contactList.size());
        for (int index = 0; index < contactList.size(); index++) {
            CountryResponse countryItem = contactList.get(index);

            if (countryItem != null && !CommonUtils.isEmpty(countryItem.getDescription()) && !CommonUtils.isEmpty(countryItem.getTitle())) {
                ListItemCountry item = new ListItemCountry();
                item.setContact(countryItem);
                listItemContacts.add(item);
            }
        }
        return listItemContacts;
    }

    @Override
    public int getType() {
        return LIST_ITEM_CONTACT;
    }
}
