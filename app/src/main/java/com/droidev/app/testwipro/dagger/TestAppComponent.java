package com.droidev.app.testwipro.dagger;

import android.app.Application;

import com.droidev.app.testwipro.dagger.module.ApplicationModule;
import com.droidev.app.testwipro.dagger.module.NetworkModule;
import com.droidev.app.testwipro.presenter.MainFragmentPresenter;
import com.droidev.app.testwipro.remote.NetworkAdapter;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = {
        NetworkModule.class,
        ApplicationModule.class
})
@TestAppScope
public interface TestAppComponent {

    void inject(NetworkAdapter networkAdapter);

    void inject(MainFragmentPresenter mainFragmentPresenter);

    @Component.Builder
    interface Builder {
        TestAppComponent build();

        @BindsInstance
        Builder application(Application application);

    }
}
