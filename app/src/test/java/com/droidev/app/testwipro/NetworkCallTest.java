package com.droidev.app.testwipro;

import com.droidev.app.testwipro.remote.NetworkAdapter;
import com.droidev.app.testwipro.remote.ResponseCallback;
import com.droidev.app.testwipro.remote.response.CountryResponse;
import com.droidev.app.testwipro.remote.response.CountryResponseWrapper;
import com.droidev.app.testwipro.remote.response.ResponseError;
import com.droidev.app.testwipro.view.MainView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class NetworkCallTest {

    private CountryResponseWrapper mCountryResponseWrapper;

    @Mock
    private NetworkAdapter mNetworkAdapter;

    @Mock
    private MainView mMainView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mCountryResponseWrapper = new CountryResponseWrapper();
        mCountryResponseWrapper.setTitle("This is test");
        List<CountryResponse> responseList = new ArrayList<>(2);
        mCountryResponseWrapper.setCountryResponses(responseList);
    }

    @Test
    public void serverCallWithSuccess() {
        mNetworkAdapter.getTestData(new ResponseCallback<CountryResponseWrapper>() {
            @Override
            public void onSuccess(CountryResponseWrapper countryResponseWrapper) {
                mMainView.populateCountryDetails(mCountryResponseWrapper);
            }

            @Override
            public void onFailure(ResponseError responseError) {
                mMainView.handleError(responseError);
            }
        });
    }

    @Test
    public void nullResponseTest(){
        mMainView.populateCountryDetails(null);
    }

    @Test
    public void failureResponseTest(){
        ResponseError error = new ResponseError();
        error.setError("Something went wrong");
        mMainView.handleError(error);
    }
}
